import { Component, OnInit } from '@angular/core';
import {FormControl, FormBuilder} from '@angular/forms';
import { Validators } from '@angular/forms';
import { User } from 'src/app/Users/userModel';
import { AuthenticationService } from 'src/app/Users/services/authentication/authentication.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material';
import { UserService } from 'src/app/Users/services/user_service/user.service';

export interface ILogin{
  userEmail: string
  userPassword: string
}

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {
  private isLoged = this.authenticationService.currentUser$.subscribe(data => this.isLoged = data.isLoged)
  private body:User
  
  constructor(
    private fb: FormBuilder,
    private authenticationService: AuthenticationService,
    private userService: UserService,
    private router: Router,
    private dialog: MatDialog,
  ) {}
  ngOnInit() {
    
  }
  signInForm = this.fb.group({
    email: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(25), Validators.email
    ]],
    password: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(25)
    ]],
  });

  deleteCurrentUser(){
    const body = {email: this.signInForm.value.email, password: this.signInForm.value.password}
    this.userService.deletetUser(body)
  }

  onSubmit() {
    const body = {email: this.signInForm.value.email, password: this.signInForm.value.password}
    
    
    this.authenticationService.login(body).subscribe(
        data => {
            console.log(localStorage.getItem('user'))
            

        },
        error => {
            alert("You’ve entered wrong email or password.");
        });

  }
}