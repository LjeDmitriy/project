import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthenticationService } from 'src/app/Users/services/authentication/authentication.service';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class JWTInterceptorService {

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService) { }
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let currentUser = this.authenticationService.currentUserValue;
    if (currentUser && currentUser.token) {
        request = request.clone({
            setHeaders: { 
                Authorization: `Bearer ${currentUser.token}`
            }
        });
    }

    return next.handle(request).pipe(tap(event => {
    },
    error => {console.log(error)
      if (HttpErrorResponse.arguments(status) == 401){this.authenticationService.logout()
     this.router.navigate(['products'])}
    alert('Session time has been end')}
     
    ));
}
}