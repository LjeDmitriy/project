import { TestBed } from '@angular/core/testing';

import { JWTInterceptorService } from './jwt-interceptor.service';

describe('JwtInterceptorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: JWTInterceptorService = TestBed.get(JWTInterceptorService);
    expect(service).toBeTruthy();
  });
});
