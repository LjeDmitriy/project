import { Injectable } from '@angular/core';
import { User } from 'src/app/Users/userModel';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthenticationService } from 'src/app/Users/services/authentication/authentication.service';

interface newUser{
  email: string,
  password: string
}

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private http: HttpClient,
  ) { }

  postUser(body: newUser){
    const url = `${environment.serverUrl}/api/v1/users`
    return this.http.post(url, body)
  }

  deletetUser(body: newUser){
    const url = `${environment.serverUrl}/api/v1/users/delete`
    return this.http.post(url, body).subscribe({
      error: error => console.error('Error', error)
    }) 
  }

  checkUser(body: User){
    const url = `${environment.serverUrl}/api/v1/check`
    return this.http.post<User>(url, body)
  }

}
