import { Injectable } from '@angular/core';
import { Router, CanActivate} from '@angular/router';


import { AuthenticationService } from 'src/app/Users/services/authentication/authentication.service';



@Injectable({
  providedIn: 'root'
})
export class AuthGuardService {
  
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService) { }
    canActivate() {
      const currentUser = this.authenticationService.currentUserValue;
      if (currentUser && currentUser.token) {
          return true;
      }
      else
      this.router.navigate(['products']);
      return false;
  }
}
