import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from 'src/app/Users/userModel';
import { UserService } from 'src/app/Users/services/user_service/user.service';

@Injectable({
  providedIn: 'root'
})
// добавить тип данных на body
export class AuthenticationService {
  private currentUserSubject: BehaviorSubject<User>;//перенести сюда присваивание
  public currentUser$: Observable<User>;
  private isLogged = new Subject<boolean>();
  public userStatus$ = this.isLogged.asObservable()


  constructor(
    private userService: UserService,
    private http: HttpClient) { 
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('user')));
        this.currentUser$ = this.currentUserSubject.asObservable();
  }
  
login(body) {
 return this.userService.checkUser(body)
        .pipe(map(user => {console.log(body)
            localStorage.setItem('user', JSON.stringify(user,body))
            console.log(localStorage.getItem('user'))
            this.currentUserSubject.next(user);
            this.isLogged.next(true)
            return user
        }));
}

public get currentUserValue(): User {
  return this.currentUserSubject.value;
}

logout() {
    localStorage.removeItem('user')
    this.currentUserSubject.next(null)
    this.isLogged.next(false)
}
}
