import { Component, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { passwordMatch } from "src/app/Users/passwordsMatch";
import { FormBuilder, FormGroup } from '@angular/forms';
import { UserService } from 'src/app/Users/services/user_service/user.service';
import { MatDialog } from '@angular/material';
import { User } from '../userModel';
import { Router } from '@angular/router';

interface IUser{
  email: string,
  password: string
}

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
 private noPasswordMatch : true
 public body: IUser

  constructor(
    private router: Router,
    private dialog: MatDialog,
    private fb: FormBuilder,
    private userService: UserService,
  ) { }

  profileForm = this.fb.group({
    email: ['', [Validators.required,Validators.maxLength(255), Validators.email
    ]],
    password: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(25)
    ]],
    passwordAgain: ['', [Validators.required, Validators.minLength(6)]],
  },{validator: passwordMatch});

  onSubmit() {
    if (this.noPasswordMatch === true){return console.error('wrong password');
    } 
    const body = {email: this.profileForm.value.email, password: this.profileForm.value.password}
    return this.userService.postUser(body).subscribe(
      data => {
          console.log("success")
          this.dialog.closeAll()
          

      },
      error => {
          alert("The user with such email already exists.");
      });
    
  }

  ngOnInit() {
  }


}
