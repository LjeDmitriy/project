import { FormControl, ValidatorFn, AbstractControl, FormGroup } from '@angular/forms';

export function passwordMatch(form: FormGroup){
  return form.controls['password'].value === 
form.controls['passwordAgain'].value ? false : form.controls['passwordAgain'].setErrors({'noPasswordMatch': true});
  }