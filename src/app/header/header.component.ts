import { Component, OnInit } from '@angular/core';
import { SignUpComponent } from '../Users/sign-up/sign-up.component';
import { MatDialog } from '@angular/material';
import { SignInComponent } from '../Users/sign-in/sign-in.component';
import { AuthenticationService } from '../Users/services/authentication/authentication.service';




@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  private isLoged = this.authenticationService.userStatus$.subscribe(data => this.isLoged = data)

  constructor(
    private dialog: MatDialog,
    private authenticationService: AuthenticationService,
  ) { 
  }

  ngOnInit() {
if (localStorage.getItem('user') == undefined){this.isLoged = false}
  }

  openModalSignUp(){
  this.dialog.open(SignUpComponent)
  }
  openModalSignIn(){
  this.dialog.open(SignInComponent)
  }

  logout() {
    this.authenticationService.logout()
    this.isLoged = false
  }

}
