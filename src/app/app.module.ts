import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS  } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { AppRoutingModule } from './app-routing.module';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';




import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { ProductsComponent } from './products/products.component';
import { ModalWindowComponent } from './products/modal_window/modal-window/modal-window.component';
import { CartComponent } from './products/cart/cart.component';
import { SignInComponent } from './Users/sign-in/sign-in.component';
import { SignUpComponent } from './Users/sign-up/sign-up.component';
//import { ProductsSearchComponent } from './products/products_search/products-search/products-search.component';
import { JWTInterceptorService } from 'src/app/Users/services/interceptors/jwt-interceptor.service'

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ProductsComponent,
    ModalWindowComponent,
    CartComponent,
    SignInComponent,
    SignUpComponent,
    //ProductsSearchComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    RouterModule,
    BrowserAnimationsModule,
    MatDialogModule,
    ScrollingModule,
    InfiniteScrollModule,
    ReactiveFormsModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JWTInterceptorService, multi: true }
  ],
  bootstrap: [AppComponent],

  entryComponents: [ ModalWindowComponent, SignUpComponent, SignInComponent ],

})
export class AppModule { }
