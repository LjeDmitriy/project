export interface Product {
  _id: string
  id: number
  image: string
  name: string
  description: string
}