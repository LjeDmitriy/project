import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/products/products_models/product';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  private selectedProducts = []

  constructor() { }

  ngOnInit() {
    if(localStorage.getItem('SingleProduct')){
    this.selectedProducts.push(JSON.parse(localStorage.getItem('SingleProduct')))
    console.log("SingleProduct",this.selectedProducts)}
    else if (localStorage.getItem('MultipleProducts')){this.selectedProducts = (JSON.parse(localStorage.getItem('MultipleProducts')))
    console.log("MultipleProducts",this.selectedProducts)}
    else console.log('Nothing to display')
  }
clearCart(): void{
localStorage.removeItem('MultipleProducts')
localStorage.removeItem('SingleProduct')
this.selectedProducts = []
}
removeFromCart(product:Product): void{
      let productToDelete = this.selectedProducts.indexOf(product,0)
    if (productToDelete != -1) {this.selectedProducts.splice(productToDelete,1)
    localStorage.setItem('MultipleProducts',JSON.stringify(this.selectedProducts))}
    else if (this.selectedProducts.length == 1){
      this.clearCart() 
}}}

