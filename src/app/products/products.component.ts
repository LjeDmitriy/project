import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/products/products_models/product';
import { ProductService } from 'src/app/products/porduct_service/product.service';
import { Subscription, Observable, Subject } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

import { MatDialog } from '@angular/material';
import { ModalWindowComponent } from '../products/modal_window/modal-window/modal-window.component';
import { AuthenticationService } from '../Users/services/authentication/authentication.service';



@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  private id: number = 0
  private notEmptyPost: boolean = true;
  public selectedProducts = []
  private subscriptions: Subscription[] = []
  public productsToDisplay = [];
  public productsToDisplay$: Observable<Product[]>;
  private isLogged = this.authenticationService.userStatus$.subscribe(data => this.isLogged = data)
  private searchTerms = new Subject<string>()
  private products = []

  constructor(
    public dialog: MatDialog,
    private route: ActivatedRoute,
    private router: Router,
    private productService: ProductService,
    private authenticationService: AuthenticationService,
  ) { }

  ngOnInit() {
    this.productService.getNextProducts(this.id).subscribe(
      data => this.productsToDisplay = data)
      if (localStorage.getItem('user') == undefined){this.isLogged = false}

      this.productsToDisplay$ = this.searchTerms.pipe(
        debounceTime(300),
        distinctUntilChanged(),
        switchMap((term: string) => this.productService.searchProducts(term))
      )
      this.productsToDisplay$.subscribe(data => {if (data.length > 0) this.productsToDisplay = data
        else this.loadNextProducts()}
        )
    }

  async loadNextProducts() {
    console.log(this.productsToDisplay)
    const lastPost = this.productsToDisplay.length;
    this.id = lastPost;
    if (this.notEmptyPost === true) {
      this.productService.getNextProducts(this.id)
        .subscribe((data:Product[]) => {
          const newPost = data;
          if (newPost.length < 1) {
            this.notEmptyPost = false;
            return
          }
          this.productsToDisplay = this.productsToDisplay.concat(newPost)
        })
    };
  }

  onScroll() {
    this.loadNextProducts()
  }

  openModal(product:Product){
    let dialogRef = this.dialog.open(ModalWindowComponent, {
      data: {
        name: product.name,
        image: product.image,
        description: product.description,
        id: product.id
      }
    }).afterClosed().subscribe(res => {
      if(res)this.selectedProducts.push(product)
    })
  }

  buyProductNow(product:Product){
    localStorage.setItem('SingleProduct', JSON.stringify(product))
    this.router.navigate(['cart'])
  }

  selectProduct(product:Product){
    let productToDelete = this.selectedProducts.indexOf(product,0)
    if (productToDelete != -1) this.selectedProducts.splice(productToDelete,1)
    else this.selectedProducts.push(product)
    console.log("Selected",this.selectedProducts)
}

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe())
  }

addToCart(): void{
  if(this.selectedProducts.length>0){
    localStorage.setItem('MultipleProducts', JSON.stringify(this.selectedProducts))}
    this.router.navigate(['cart'])
}

  search(term: string): void {
    this.searchTerms.next(term);
    if (!term) this.productsToDisplay = []
  }

}


