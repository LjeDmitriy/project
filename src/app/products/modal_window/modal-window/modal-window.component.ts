import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ProductsComponent } from 'src/app/products/products.component';
import { AuthenticationService } from 'src/app/Users/services/authentication/authentication.service';

@Component({
  selector: 'app-modal-window',
  templateUrl: './modal-window.component.html',
  styleUrls: ['./modal-window.component.scss'],
})
export class ModalWindowComponent implements OnInit {
  private isLogged = this.authenticationService.userStatus$.subscribe(data => this.isLogged = data)

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<ModalWindowComponent>,
    private authenticationService: AuthenticationService,
  ) { }

  ngOnInit() {
    if (localStorage.getItem('user') == undefined){this.isLogged = false}
  }

addToCart(){
  this.dialogRef.close({data:'data'})
}

}
