import { Component, OnInit } from '@angular/core';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

import { Product } from 'src/app/products/products_models/product';

export interface IProductsResult {
  data: Product[],
}

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(
    private http: HttpClient,
  ) { }


getAllProducts(): Observable<Product[]> {
  const url = `${environment.serverUrl}/api/v1/products`
  return this.http.get<IProductsResult>(url)
    .pipe(
      map(result =>{ 
        return result.data})
    );
}

getNextProducts(id:number): Observable<Product[]> {
  const url = `${environment.serverUrl}/api/v1/product/${id}`
  return this.http.get<IProductsResult>(url).pipe(
    map(result =>{ console.log(result.data)
      return result.data
    }
      ))
}

getSingleProduct(id:number): Observable<Product> {
  const url = `${environment.serverUrl}/api/v1/product/${id}`
  return this.http.get<Product>(url)
    }
      
searchProducts(term: string): Observable<Product[]> {
  if (!term.trim()) {
    return of([]);
  }
  return this.http.get<IProductsResult>(`${environment.serverUrl}/api/v1/products/${term}`)
  .pipe(
    map(result =>{
      return result.data})
  );
}

}